const { sqsInquiriesQueueName } = require('../../constants')

function processInquiry(sqs, payload) {
    return sqs.getQueueUrl({ QueueName: sqsInquiriesQueueName })
        .promise()
        .then(data => ({
            MessageAttributes: {
                "firstName": {
                    DataType: "String",
                    StringValue: payload.firstName
                },
                "lastName": {
                    DataType: "String",
                    StringValue: payload.lastName
                },
                "email": {
                    DataType: "String",
                    StringValue: payload.email
                },
                "phone": {
                    DataType: "String",
                    StringValue: payload.phone
                },
                "postcode": {
                    DataType: "String",
                    StringValue: payload.postcode
                },

            },
            MessageBody: JSON.stringify(payload),
            QueueUrl: data.QueueUrl
        }))
        .then(params => sqs.sendMessage(params).promise())
}

module.exports = processInquiry;