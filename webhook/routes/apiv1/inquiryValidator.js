const { Validator } = require("express-json-validator-middleware");
const validator = new Validator()

const inquirySchema = {
    type: 'object',
    required: ['firstName', 'lastName', 'email', 'phone', 'postcode'],
    properties: {
        firstName: {
            type: 'string',
            allOf: [
                { transform: ['trim'] },
                { minLength: 1 }
            ]
        },
        lastName: {
            type: "string",
            allOf: [
                { transform: ['trim'] },
                { minLength: 1 }
            ]
        },
        email: {
            type: "string",
            format: 'email',
            transform: ['trim']
        },
        phone: {
            type: "string",
            minLength: 1,
            allOf: [
                { transform: ['trim'] },
                { minLength: 1 }
            ]
        },
        postcode: {
            type: "string",
            allOf: [
                { transform: ['trim'] },
                { minLength: 6, maxLength: 8 }
            ]
        },
    },
    additionalProperties: false
};

require("ajv-keywords")(validator.ajv, ["transform"])

module.exports = validator.validate({ body: inquirySchema })