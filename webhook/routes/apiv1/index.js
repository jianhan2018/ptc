const { Router } = require('express');
const router = Router();
const inquiry = require('./inquiry');

router.use('/inquiry', inquiry);
module.exports = router;
