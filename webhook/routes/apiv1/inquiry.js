const express = require('express');
const router = express.Router();
const validator = require('./inquiryValidator');
const { StatusCodes } = require('http-status-codes');
const inquiryProcessor = require('./inquiryProcessor');
const logger = require('../../logger');

router.post('/', validator, function (req, res) {
    inquiryProcessor(req.sqs, req.body).then(data => {
        logger.info(`inquiry persisted: ${JSON.stringify(req.body)}, message id: ${data.MessageId}`)
        res.status(StatusCodes.ACCEPTED).json();
    }).catch(err => {
        logger.error(`error: ${err.toString()}`)
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    })
})

module.exports = router;