const chai = require('chai');
const chaiHttp = require('chai-http');
const { StatusCodes } = require('http-status-codes');

const app = require('../app');
chai.should()
chai.use(chaiHttp);


// TODO: test inquiry can be successfully persisted into sqs queue

const payload = {
  "firstName": "test first name",
  "lastName": "test last name",
  "email": "test@gmail.com",
  "phone": "011234556",
  "postcode": "WC2N 5DU"
}

describe('inquiry webhook endpoint tests', () => {

  /**
   * set up test cases for testing validation rules
   */
  let testCases = [
    { payload: Object.assign({}, payload, { 'firstName': '' }) },
    { payload: Object.assign({}, payload, { 'firstName': ' ' }) },
    { payload: Object.assign({}, payload, { 'lastName': '' }) },
    { payload: Object.assign({}, payload, { 'lastName': ' ' }) },
    { payload: Object.assign({}, payload, { 'phone': '' }) },
    { payload: Object.assign({}, payload, { 'phone': ' ' }) },
    { payload: Object.assign({}, payload, { 'email': '' }) },
    { payload: Object.assign({}, payload, { 'email': ' ' }) },
    { payload: Object.assign({}, payload, { 'email': 'invalid email' }) },
    { payload: Object.assign({}, payload, { 'postcode': '' }) },
    { payload: Object.assign({}, payload, { 'postcode': ' ' }) },
    { payload: Object.assign({}, payload, { 'postcode': 'this postcode is too long' }) },
    { payload: Object.assign({}, payload, { 'postcode': 'a' }) },
  ];

  testCases.forEach(({ payload }) => {

    it(`it should return 400 with payload ${JSON.stringify(payload)}`, async function () {
      const response = await chai.request(app)
        .post('/api/v1/inquiry')
        .type('json')
        .send(payload);
      response.should.have.status(StatusCodes.BAD_REQUEST);
    })
  });

  /**
   * set up test cases for testing invalid Content-type headers
   */
  testCases = [
    { type: 'form', payload: payload },
    { type: 'html', payload: '<html>test</html>' },
    { type: 'xml', payload: '<xml></xml>' },
    { type: 'text', payload: 'test' },
  ];

  testCases.forEach(({ type, payload }) => {

    it(`it should return 400 with payload ${JSON.stringify(payload)}`, async function () {
      const response = await chai.request(app)
        .post('/api/v1/inquiry')
        .type(type)
        .send(payload);
      response.should.have.status(StatusCodes.BAD_REQUEST);
    })
  });

});
