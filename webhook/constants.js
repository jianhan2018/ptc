const sqsEndpoint = process.env.SQS_ENDPOINT || 'http://localhost:9324';
const sqsRegion = process.env.SQS_REGION || 'ap-southeast-2';
const sqsAccessKeyId = process.env.SQS_ACCESS_KEY_ID || 'access_key_id';
const sqsSecretAccessKey = process.env.SQS_SECRET_ACCESS_KEY || 'secret_access_key';
const sqsInquiriesQueueName = process.env.SQS_INQUIRIES_QUEUE_NAME || 'inquiries-queue';

module.exports = {
    sqsEndpoint,
    sqsRegion,
    sqsAccessKeyId,
    sqsSecretAccessKey,
    sqsInquiriesQueueName
}