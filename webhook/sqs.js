const { SQS } = require('aws-sdk');
const {
    sqsEndpoint,
    sqsRegion,
    sqsAccessKeyId,
    sqsSecretAccessKey
} = require('./constants')

let sqs = null;

function getSqs() {
    if (!sqs) {
        sqs = new SQS({
            endpoint: sqsEndpoint,
            region: sqsRegion,
            accessKeyId: sqsAccessKeyId,
            secretAccessKey: sqsSecretAccessKey,
        });
    }
    return sqs;
}

module.exports = getSqs;