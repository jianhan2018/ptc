const { StatusCodes, ReasonPhrases } = require('http-status-codes');
const { ValidationError } = require("express-json-validator-middleware");
const { createLogger, format, transports } = require('winston');
const { combine, splat, timestamp, printf } = format;
const expressWinston = require('express-winston');
const getSqs = require('./sqs')
const logger = require('./logger');

const myFormat = printf(({ level, message, timestamp, ...metadata }) => {
    let msg = `${timestamp} [${level}] : ${message} `
    if (metadata) {
        msg += JSON.stringify(metadata)
    }
    return msg
});

const requestLogger = expressWinston.logger({
    transports: [
        new transports.Console()
    ],
    format: combine(
        format.colorize(),
        splat(),
        timestamp(),
        myFormat
    ),
    meta: true,
    expressFormat: true,
})

function sqsConnectionInjection(req, res, next) {
    req.sqs = getSqs();
    next();
}

function invalidContentTypeRequestHandler(req, res, next) {
    if (!req.is('application/json')) {
        logger.warn(`invalid content type: ${req.headers['content-type']}`)
        res.status(StatusCodes.BAD_REQUEST).json();
        return;
    }

    next();
}

function notFoundRequestHandler(req, res, next) {
    logger.warn(`resource not found`)
    res.status(StatusCodes.NOT_FOUND);
    res.json()
}

function validationErrHandler(err, req, res, next) {
    if (err instanceof ValidationError) {
        logger.warn(`invalid request: `, err)
        res.status(StatusCodes.BAD_REQUEST).json();
        return;
    }
    next(err);
}

function internalServerErrHandler(err, req, res, next) {
    logger.error(`internal server err: ${err}`)
    res.status(err.status || StatusCodes.INTERNAL_SERVER_ERROR);
    res.json();
}


module.exports = {
    invalidContentTypeRequestHandler,
    notFoundRequestHandler,
    validationErrHandler,
    internalServerErrHandler,
    sqsConnectionInjection,
    requestLogger
};