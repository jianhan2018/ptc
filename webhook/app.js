const cookieParser = require('cookie-parser');
const express = require('express');
const {
  sqsConnectionInjection,
  invalidContentTypeRequestHandler,
  notFoundRequestHandler,
  validationErrHandler,
  internalServerErrHandler,
  requestLogger
} = require('./middleware');

const app = express();
const apiv1 = require('./routes/apiv1');

// setup global middleware
app.use(requestLogger)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sqsConnectionInjection)

// define api routes and middleware
app.use('/api/', invalidContentTypeRequestHandler);
app.use('/api/v1', apiv1);
app.use(notFoundRequestHandler);
app.use(validationErrHandler);
app.use(internalServerErrHandler);

module.exports = app;
