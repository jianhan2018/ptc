# ptc

## High Level Overview
![alt text](high-level.png)

As illustrated above, the system contains 2 services and elasticMQ.
1. Webhook service
   - A dockerized Node.js web application implemented by Express.js to handle HTTP requests from clients. 
2. ElasticMQ
    - An existing docker image pulled from [softwaremill/elasticmq](https://hub.docker.com/r/softwaremill/elasticmq) 
3. Email service
    - A dockerized Node.js application that polls messages from the ElasticMQ service.  

Docker compose will be used to set up and run all those services.

## Requirements fulfillments
This section lists requirements that have been fulfilled according to those 3 services above.
1. Webhook service:
   - Endpoint expose an HTTP endpoint that receives a POST request with a specific JSON format.
   - Payload should be safely persisted in the queue, then respond with a `202 - Accepted` status code.
   - The server should expect content to be the exact structure with content-type `application/json`, anything else should be rejected with `400 - Bad Request`
   - Additionally, the service also validates that all fields can not be an empty string, email must be in a valid email format and postcode length must be correct.
2. ElasticMQ
   - Persist message submitted by webhook service.
3. Email service
   - Poll messages from the queue.
   - Choose a template based on the postcode of inquiry, all postcode that starts with `NW8` will use a `accept` template, anything else uses `decline` template. 
   - Message within the email was composed according to the content within the inquiry.
   - Send email via a mocked function with delay.
4. Build
   - Webhook service and email service can be built into separated docker images by corresponding docker files of each service.
   - A make file has been created to build those 2 services above with a single command, and the output artifacts are docker images that are versioned based on the git hash.
5. Deploy
    1. Since lack of experience in Kubernetes (I assume it will be used for deployment), only docker-compose is used.
    2. I only have previous experience in Terraform + CI/CD pipeline setup within gitlab and github for deployment.

## How to run
### Prerequisites
1. The only application needed on the host machine is Docker since all services will be run as a docker container.
2. Ensure the ports `3001`, `9324` and `9325` are not in use.

### Spin up all services and test locally
A make file is used as a single point of interaction for tasks such as build/start containers, running unit tests, building individual services, etc.. It is just a convenient thin wrapper.

1. Clone the repo.
2. To start all services, run `make up`.
3. To make an inquiry POST HTTP request, the endpoint is `http://localhost:3001/api/v1/inquiry`.
4. To monitor the messages in the queue, navigate to `http://localhost:9325/`.
5. To inspect the services, run `make logs-webhook` or `make logs-email`.
6. For all other commands, run `make` and play with them, should be self-explanatory.
7. In order to easily inspect how the message is flowing from service to service, there are 2 commands that can tail the logs for webhook service and email service: `make logs-webhook` and `make logs-email`.

To sum up, after cloning the repo, run `make up`, tail the logs by `make logs-webhook` and `make logs-email`, at last, make the HTTP post request to the endpoint, the messages should show in logs within both services.

## Extra notes & technical decisions were made behind the implementation
During the implementation, it is worth mentioning why webhook and email service was implemented in the way they were, so that thought processes can be clearly illustrated.

1. Webhook service
   - The reason why node and express framework was chosen is that they are simple, minimalist and a good fit for developing API endpoint.
2. Email service
   - Rxjs library was used so that asynchronous logic can be easily composed and managed, this is a normal CLI app that was implemented in functional-style programming. Also, extra loggings has been added so that how the message is processed is very clear, feel free to change some of the code and play with it if you are interested.
3. Further improvements

   The current state of the system is in a working state, however, there are some further improvements that can be made:
   1. Benchmark testing for services can be performed to identify if there are any performance bottlenecks. Even if those services are very simple in nature, however, I will never know if they would run into performance issues without benchmark testing.
   2. Optimize docker image size, the docker file for both services are minimalist, optimizations can be done in order to reduce the image sizes such as only installing production dependencies and multi-stage build. At the same time, fix the warning during building the images.
   3. More sophisticated error handling and logging for both services. Good error handlings and loggings are very important when it comes to monitoring and debugging.
      1. For instance define an environment variable, and log level so that the service can log useful information depending on the environment.
      2. When it comes to error handling, a few more edge cases can be covered such as invalid JSON payload in the queue.
   4. More unit tests. 
   5. E2E tests.

## Final Thoughts
All services have been tested on 2 machines:
1. Laptop:
    - Ubuntu 20.04.2 LTS
    - Docker version 19.03.12, build 48a66213fe
    - docker-compose version 1.29.2, build 5becea4c
2. Desktop
    - Ubuntu 19.04
    - Docker version 19.03.6, build 369ce74a3c
    - docker-compose version 1.21.0, build unknown

Both run fine, however, if any issue occurs please feel free to get in touch and will fix it asap.