webhook_service_name := jianhan/webhook
email_service_name := jianhan/email
tag := $$(git log -1 --pretty=format:"%h")
webhook_service_img := ${webhook_service_name}:${tag}
email_service_img := ${email_service_name}:${tag}

help: ## A build tool for the app.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
 
test: test-webhook test-email ## run all unit tests for webhook and email service
	@echo "--- finished tests for all services ---"

test-webhook: ## run unit tests for webhook service
	@echo "--- running tests for webhook service ---"
	docker-compose run --entrypoint="npm run test:unit" webhook

test-email: ## run unit tests for email service
	@echo "--- running tests for email service ---"
	docker-compose run --entrypoint="npm run test:unit" email

build: build-webhook build-email ## build all services
	@echo "--- finished building for all services ---"

build-webhook: ## build webhook service
	@echo "--- start to build $(webhook_service_img) ---"; \
	cd webhook; \
	docker build -t $(webhook_service_img) .

build-email: ## build email service
	@echo "--- start to build $(email_service_img) ---"; \
	cd email; \
	docker build -t $(email_service_img) .

logs-webhook: ## show logs for webhook service
	docker-compose logs -f webhook

logs-email: ## show logs for email service
	docker-compose logs -f email

up: ## a thin wrapper to run docker-compose up
	@echo "--- running docker-compose up -d ---"
	docker-compose up -d

down: ## a thin wrapper to run docker-compose down
	@echo "--- running docker-compose down ---"
	docker-compose down

stop: ## a thin wrapper to run docker-compose stop
	@echo "--- running docker-compose stop ---"
	docker-compose stop

ps: ## a thin wrapper to run docker-compose ps
	@echo "--- running docker-compose ps ---"
	docker-compose ps

restart: ## a thin wrapper to run docker compose stop and then up
	@echo "--- running docker-compose ps ---"
	docker-compose stop
	docker-compose up -d