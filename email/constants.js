const QUEUE_NAME = process.env.SQS_QUEUE_NAME || 'inquiries-queue';
const ONE_SECOND = 1000;
const THREE_SECONDS = 3000;
const FIVE_SECONDS = 5000;
const HALF_SECOND = 500;
const VALID_POSTCODE_PREFIX = 'NW8';

module.exports = {
    QUEUE_NAME,
    HALF_SECOND,
    ONE_SECOND,
    THREE_SECONDS,
    FIVE_SECONDS,
    VALID_POSTCODE_PREFIX
}