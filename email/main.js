const logger = require('./logger')
const sqs = require('./sqs');
const { of, tap } = require('rxjs');
const { catchError, mergeAll } = require('rxjs/operators');
const { QUEUE_NAME, THREE_SECONDS } = require('./constants');
const { fetchQueueUrl, pollMessagesEvery, transformMessages, randomDelay, sendEmail, deleteMessage } = require('./functions')


// kick off the app
fetchQueueUrl(sqs, QUEUE_NAME).pipe(
    tap(data => logger.info(`service started, resolved queue url ${data} and start polling...`)),
    pollMessagesEvery(THREE_SECONDS),
    tap(data => logger.info(`finished polling, ${data.Messages ? data.Messages.length : 0} message(s) received`)),
    transformMessages,
    tap(data => data.length === 0 ? null : logger.info(`${data.length} message transformed`)),
    mergeAll(), // flatten inner message observables  
    randomDelay,
    tap(data => logger.info(`generated delay of ${data.delay}ms for message`, data.message.Body)),
    sendEmail,
    tap(data => logger.info(`${data.Template} email has been sent to ${data.Body.email} with content "${data.EmailContent}"`)),
    deleteMessage,
    tap(data => logger.info(`message has been deleted`, data.Body)),
    catchError(of)
).subscribe({
    next: (data) => logger.info(`finished message processing`, data.Body),
    error: (e) => logger.error(`error ${e}`),
    complete: () => logger.info('complete')
})