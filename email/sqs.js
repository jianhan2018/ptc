const { SQS } = require('aws-sdk');

const sqs = new SQS({
    endpoint: process.env.SQS_ENDPOINT || 'http://localhost:9324',
    region: process.env.SQS_REGION || 'ap-southeast-2',
    accessKeyId: process.env.SQS_ACCESS_KEY_ID || 'access_key_id',
    secretAccessKey: process.env.SQS_SECRET_ACCESS_KEY || 'secret_access_key',
})

module.exports = sqs;