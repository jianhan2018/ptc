const { timer, from } = require('rxjs');
const { mapTo, switchMap, mergeMap, map: rxMap } = require('rxjs/operators');
const { HALF_SECOND, ONE_SECOND, VALID_POSTCODE_PREFIX } = require('./constants');
const { pipe, ifElse, prop, map, pick, over, lens, assoc, pathSatisfies, startsWith, always, isEmpty, complement } = require('ramda');
const sqs = require('./sqs');

/**
 * emailTemplates define two different templates when sending email.
 */
const emailTemplates = {
    'accept': ({ firstName, lastName, postcode }) =>
        `Deal ${firstName} ${lastName} @ ${postcode}, thanks for your inquiry, we will get back to you shortly.`,
    'decline': ({ firstName, lastName, postcode }) =>
        `Deal ${firstName} ${lastName} @ ${postcode}, thanks for your inquiry, our service is not available in your area.`
}

/**
 * fetch queue url by queue name.
 */
function fetchQueueUrl(sqs, queueName) {
    return from(sqs.getQueueUrl({ QueueName: queueName }).promise().then(data => data.QueueUrl));
}

/**
 * receive messages from the queue.
 */
function receiveMessage(sqs) {
    return function (queueUrl) {
        const params = {
            MaxNumberOfMessages: 10,
            MessageAttributeNames: [
                "All"
            ],
            QueueUrl: queueUrl,
        };
        return from(sqs.receiveMessage(params).promise().then(data => Object.assign({}, { QueueUrl: queueUrl }, data)));
    }
}

/**
 * generate a random number, will be used to simulate random delay of sending email.
 */
function randomBetween(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

/**
 * aws sdk does not allow to attach a handler to listen on event, thus poll messages.
 */
function pollMessagesEvery(interval) {
    const pollMessages = function (queueUrl) {
        return timer(0, interval).pipe(mapTo(queueUrl), switchMap(receiveMessage(sqs)));
    };

    return switchMap(pollMessages);
}

// only need 'Body', 'MessageAttributes' and 'ReceiptHandle' from message.
const pickMessageBodyAttr = pick(['Body', 'MessageAttributes', 'ReceiptHandle']);

// lens used to set the attribute of body.
const messageBodyLens = lens(prop('Body'), assoc('Body'));

// parse body json string to object.
const parseJsonBody = over(messageBodyLens, JSON.parse);

// assign Template attribute to Body according to postcode.
const updateEmailTemplate = ifElse(
    pathSatisfies(startsWith(VALID_POSTCODE_PREFIX), ['Body', 'postcode']),
    assoc('Template', 'accept'),
    assoc('Template', 'decline'),
)

/**
 * transformMessages transforms messages returned from queue, and update message
 * with extra information for subsequent operations such as sending email and
 * deleting message.
 */
function transformMessages({ QueueUrl: queueUrl, Messages: messages = [] }) {
    return pipe(
        ifElse(
            complement(isEmpty),
            map(
                pipe(
                    pickMessageBodyAttr, // only pick attributes needed
                    parseJsonBody, // parse body
                    updateEmailTemplate, // update message with template accept/decline
                    assoc('MessageSent', false), // indicate if email has been sent
                    assoc('MessageDeleted', false), // indicate if message has been deleted
                    assoc('QueueUrl', queueUrl), // queueUrl used for deleting message
                )
            ),
            always([]),
        )
    )(messages)
}

/**
 * this is a dummy function to simulate sending email with delay. 
 */
function sendEmail({ delay, message }) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            const emailContent = emailTemplates[message['Template']](message.Body);
            message['MessageSent'] = true;
            message['EmailContent'] = emailContent;
            resolve(message);
        }, delay);
    })
}

/**
 * delete message from queue after email has been sent.
 */
function deleteMessage(sqs) {
    return function (message) {
        const deleteParams = {
            QueueUrl: message.QueueUrl,
            ReceiptHandle: message.ReceiptHandle
        };
        message['MessageDeleted'] = true;
        return sqs.deleteMessage(deleteParams).promise().then(resp => Object.assign({}, resp, message));
    }
}

/**
 * generate random delay to simulate email sending API delay.
 */
function generateRandomDelay(min, max) {
    return function (message) {
        const delay = randomBetween(min, max);
        return { delay, message }
    }
}

module.exports = {
    fetchQueueUrl,
    pollMessagesEvery,
    transformMessages: rxMap(transformMessages),
    randomDelay: rxMap(generateRandomDelay(HALF_SECOND, ONE_SECOND)),
    sendEmail: mergeMap(sendEmail),
    deleteMessage: mergeMap(deleteMessage(sqs))
}