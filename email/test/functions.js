const { expect } = require('chai')
const AWSMock = require('aws-sdk-mock');
const { of } = require('rxjs');
const { VALID_POSTCODE_PREFIX } = require('../constants')
const expectedQueueUrl = 'http://localhost/test';

describe('test fetchQueueUrl function', () => {

    beforeEach(() => {
        AWSMock.mock('SQS', 'getQueueUrl', (params) => Promise.resolve({ QueueUrl: expectedQueueUrl }));
    })

    afterEach(() => {
        AWSMock.restore('SQS', 'getQueueUrl');
    })

    it('fetchQueueUrl should return queue url', async () => {
        const { fetchQueueUrl } = require('../functions')
        const sqs = require('../sqs')
        const actualQueueUrl = await fetchQueueUrl(sqs, 'test').toPromise()
        expect(actualQueueUrl).to.equal(expectedQueueUrl)
    })
})

describe('test transformMessages function', () => {

    it('should return empty array when messages are empty', async () => {
        const { transformMessages } = require('../functions')
        const withoutMessages = {
            QueueUrl: 'http://localhost:9324/000000000000/inquiries-queue',
            ResponseMetadata: { RequestId: '00000000-0000-0000-0000-000000000000' }
        };

        const emptyMessages = {
            QueueUrl: 'http://localhost:9324/000000000000/inquiries-queue',
            ResponseMetadata: { RequestId: '00000000-0000-0000-0000-000000000000' },
            Messages: []
        };

        [withoutMessages, emptyMessages].forEach(async (input) => {
            const actualOutput = await of(input).pipe(transformMessages).toPromise()
            expect(actualOutput).to.have.lengthOf(0)
        })
    })

    describe('output message should have required attributes', () => {
        const messages = {
            QueueUrl: 'http://test.local/queue/123',
            ResponseMetadata: { RequestId: '00000000-0000-0000-0000-000000000000' },
            Messages: [
                {
                    MessageId: '1',
                    ReceiptHandle: '1',
                    MD5OfBody: 'e68822970838e125674a0b1b10b53873',
                    Body: '{"firstName":"Adam","lastName":"London","email":"al@gmail.com","phone":"123456","postcode":"NW8 5DU"}',
                    MD5OfMessageAttributes: '2c35150a097a9bad6b5162130d667900',
                    MessageAttributes: {}
                },
                {
                    MessageId: '2',
                    ReceiptHandle: '2',
                    MD5OfBody: 'e68822970838e125674a0b1b10b53873',
                    Body: '{"firstName":"Bob","lastName":"Manchester","email":"bm@gmail.com","phone":"123456","postcode":"M2 4WU"}',
                    MD5OfMessageAttributes: '2c35150a097a9bad6b5162130d667900',
                    MessageAttributes: {}
                },
                {
                    MessageId: '3',
                    ReceiptHandle: '3',
                    MD5OfBody: 'e68822970838e125674a0b1b10b53873',
                    Body: '{"firstName":"Charlie","lastName":"Liverpool","email":"cl@gmail.com","phone":"123456","postcode":"L2 2DP"}',
                    MD5OfMessageAttributes: '2c35150a097a9bad6b5162130d667900',
                    MessageAttributes: {}
                }
            ]
        };

        it('it should have all required attributes', async () => {
            const { transformMessages } = require('../functions')
            const actualMessages = await of(messages).pipe(transformMessages).toPromise();
            actualMessages.forEach((actualMessage) => {
                // asserts properties exists
                expect(actualMessage).to.have.property('Body');
                expect(actualMessage).to.have.property('MessageAttributes');
                expect(actualMessage).to.have.property('ReceiptHandle');
                expect(actualMessage).to.have.property('QueueUrl');
                expect(actualMessage).to.have.property('MessageSent');
                expect(actualMessage).to.have.property('MessageDeleted');

                // asserts property values
                expect(actualMessage.Body).to.be.instanceOf(Object);
                expect(actualMessage.QueueUrl).to.equal(messages.QueueUrl);
                expect(actualMessage.MessageSent).to.be.false;
                expect(actualMessage.MessageDeleted).to.be.false;
                const expectedPostcode = actualMessage.Body.postcode.startsWith(VALID_POSTCODE_PREFIX) ? 'accept' : 'decline';
                expect(actualMessage.Template).to.be.equal(expectedPostcode);
            })
        })
    })
})